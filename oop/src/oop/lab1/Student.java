package oop.lab1;

//TODO Write class Javadoc
/**
 * A simple model for a student that is a subclass of Person.
 * 
 * @author Chinatip Vichian
 */
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * Initialize a new Student object.
	 * @param name is the name of the new Student
	 * @param id is the identifier of the new Student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person kk
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	} 
	//TODO Write equals
	
	public boolean equals(Object other) {
		if(other == null)
			return false;
		if(other.getClass() != this.getClass())
			return false;

		if (id == ((Student)other).id)
			return true;
		return false; 
	}
}
